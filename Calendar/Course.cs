﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calendar
{
    class Course
    {
        private string courseCode;
        private string courseName;
        private string description;
        private string corequisite;
        private string prerequisite;
        private string exclusion;
        private string distributionRequirement;  // Distribution Requirement Status
        private int[] breadthRequirement;  // Breadth Requirement

        public Course(string courseCode, string courseName, string description, 
                        string corequisite, string prerequisite, string exclusion, string dr, int[] br)
        {
            this.courseCode = courseCode;
            this.courseName = courseName;
            this.description = description;
            this.corequisite = corequisite;
            this.prerequisite = prerequisite;
            this.exclusion = exclusion;
            this.distributionRequirement = dr;
            this.breadthRequirement = br;
        }

        public string getCourseCode()
        {
            return this.courseCode;
        }

        public string getCourseName()
        {
            return this.courseName;
        }

        public string getDescription()
        {
            return this.description;
        }

        public string getCorequisite()
        {
            return this.corequisite;
        }

        public string getPrerequisite()
        {
            return this.prerequisite;
        }

        public string getExclusion()
        {
            return this.exclusion;
        }

        public string getDistributionRequirement()
        {
            return this.distributionRequirement;
        }

        public int[] getBreadthRequirement()
        {
            return this.breadthRequirement;
        }

        public override string ToString()
        {
            string nl = "\r\n";
            string ret = this.courseCode + "    " + this.courseName + nl + this.description;
            if (this.prerequisite.Length > 0)
                ret += (this.prerequisite + nl);
            if (this.corequisite.Length > 0)
                ret += (this.corequisite + nl);
            if (this.exclusion.Length > 0)
                ret += (this.exclusion + nl);
            if (this.distributionRequirement.Length > 0)
                ret += (this.distributionRequirement + nl);
            if (this.breadthRequirement.Length > 0)
            {
                if (this.breadthRequirement.Length == 1)
                {
                    switch (this.breadthRequirement[0])
                    {
                        case 1:
                            ret += ("Creative and Cultural Representations (1)" + nl);
                            break;
                        case 2:
                            ret += ("Thought, Belief, and Behaviour (2)" + nl);
                            break;
                        case 3:
                            ret += ("Society and Its Institutions (3)" + nl);
                            break;
                        case 4:
                            ret += ("Living Things and Their Environment (4)" + nl);
                            break;
                        case 5:
                            ret += ("The Physical and Mathematical Universes (5)" + nl);
                            break;
                    }
                }
                else
                {
                    for (int i = 0; i < this.breadthRequirement.Length; ++i)
                    {
                        switch (this.breadthRequirement[0])
                        {
                            case 1:
                                ret += ("Creative and Cultural Representations (1)" + nl);
                                break;
                            case 2:
                                ret += ("Thought, Belief, and Behaviour (2)" + nl);
                                break;
                            case 3:
                                ret += ("Society and Its Institutions (3)" + nl);
                                break;
                            case 4:
                                ret += ("Living Things and Their Environment (4)" + nl);
                                break;
                            case 5:
                                ret += ("The Physical and Mathematical Universes (5)" + nl);
                                break;
                        }
                    }
                }
            }
            return ret;
        }

        public override bool Equals(object obj)
        {
            if (obj.GetType().Name == "Course")
            {
                Course c = (Course)obj;
                return this.courseCode == c.courseCode && this.courseName == c.courseName 
                    && this.description == c.description && this.corequisite == c.corequisite 
                    && this.exclusion == c.exclusion && this.distributionRequirement == c.distributionRequirement
                    && this.breadthRequirement == c.breadthRequirement;
            }
            else
                return false;
        }
    }
}