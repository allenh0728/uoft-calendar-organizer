﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Text.RegularExpressions;

namespace Calendar
{
    class Parser
    {
        private readonly string calendarURL = "http://www.artsandscience.utoronto.ca/ofr/calendar/";

        public void update()
        {
            string[] url = getURL();
            foreach (string s in url)
            {
                WebClient client = new WebClient();
                string content = client.DownloadString(s);

            }

        }

        private string[] getURL()
        {
            string regex = "(<a href=\"(.*?)\">)([A-Za-z -().,&amp;:]+)(</a>)";
            WebClient client = new WebClient();
            string content = client.DownloadString(calendarURL);
            Match match = Regex.Match(content, regex);
            List<string> url = new List<string>();

            while (match.Success)
            {
                if (match.Groups[2].ToString().Contains("crs"))
                {
                    url.Add(calendarURL + match.Groups[2].ToString());   
                }
                match = match.NextMatch();
            }
            string[] ret = url.ToArray<string>();

            return ret;
        }

        private void getCourseInformation(string content)
        {
        }

        private string getTitle(string content)
        {
            string regex = "(<h1>)(.*?)(</h1>)";
            Match match = Regex.Match(content, regex);
            string title = "";
            if (match.Success)
            {
                title = match.Groups[2].ToString();
            }
            return title;
        }
    }
}
